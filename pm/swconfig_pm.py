#! /usr/bin/env python

"""
@title SW-Config Process Maintainer (swconfig_pm.py)
@author Squirrely
@version v0.1.2
@version_date 16 December 2013
@description This file was created by SW-Config. It is run on a cron job to check if specified processes are running and starts them. (Incase the processes got shut down unexpectedly or similar.)
@license swconfig_pm.py by Squirrely is licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License. Please see http://creativecommons.org/licenses/by-nc/3.0/
@license_attribution If you redistribute/remix this script all I ask is you please give credit where it is deserved.
"""

from swconfig import *
import os

def main():

	# If you wish to add more processes to auto start, add them here.
	processes = ["rtorrent", "irssi"]
	
	for p in processes:
		if (not is_process_running(p)):
			run_command("screen -dmS %s %s" % (p[:2], p))

if __name__ == "__main__":
	main()